function SpaceShip(name, topSpeed) {
    this.name = name;
    this.topSpeed = topSpeed;
    this.accelerate = function() {
        console.log(this.name + ' moving to ' + this.topSpeed);
    }



};


// Create a constructor function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`



// Make name and topSpeed private

function SpaceShip(name, topSpeed) {
    var name = name;
    var topSpeed = topSpeed;
    this.accelerate = function() {
        console.log(name + ' moving to ' + topSpeed);
    }



};

// Keep both name and topSpeed private, but 
// add a method that changes the topSpeed

function SpaceShip(name, topSpeed) {
    var name = name;
    var topSpeed = topSpeed;
    this.accelerate = function() {
        console.log(name + ' moving to ' + topSpeed);
    }
    this.throttle = function(newSpeed) {
        topSpeed = newSpeed;
    }



};

// Call the constructor with a couple ships, change the topSpeed
// using the method, and call accelerate.
var enterprise = ('Enterprise NCC-1701', 'Warp 5');
var millieniumFalcon = ('Millenium Falcon', '.5 past Lightspeed');
enterprise.throttle('Warp 7');
enterprise.accelerate();