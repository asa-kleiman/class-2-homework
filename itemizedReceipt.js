// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price
function logReceipt(food) {
    var taxRate = arguments[arguments.length - 1];
    var costTotal = 0;
    for (i = 0; i < arguments.length - 1; i++) {
        itemPrice = arguments[i].price;
        console.log(arguments[i].descr + ' - ' + itemPrice);
        costTotal = costTotal + itemPrice;
    };
    taxTotal = parseFloat(costTotal * taxRate).toFixed(2);
    fullTotal = parseFloat(parseFloat(costTotal) + parseFloat(taxTotal)).toFixed(2);
    console.log('Subtotal: ' + costTotal);
    console.log('Tax: ' + taxTotal);
    console.log('Total: ' + fullTotal);
    console.log('Please come again!');

};

// Check
logReceipt({ descr: 'Burrito', price: 5.99 }, { descr: 'Chips & Salsa', price: 2.99 }, { descr: 'Sprite', price: 1.99 }, .05);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97