const assignmentDate = '1/21/2019';

// Convert to a Date instance
var cutDate = assignmentDate.split("/");
var parsedDate = new Date(cutDate[2], (cutDate[0] - 1), cutDate[1]);

// Create dueDate which is 7 days after assignmentDate
//get the miliseconds in seven days
var dueDays = 86400000 * 7;
var dueDate = new Date(parsedDate + dueDays);

// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// log this value using console.log
//Turn dueDate into a series of strings
//First words
var outputDate = dueDate.toString().split(" ");
var displayDate = outputDate[1] + ' ' + outputDate[2] + ', ' + outputDate[3];
//now numbers

var numericalYear = dueDate.getUTCFullYear();
var numericalMonth = dueDate.getUTCMonth() + 1;
var numericalDay = dueDate.getUTCDate();
var numericalDate = numericalYear + '-' + numericalMonth + '-' + numericalDay;

//mash it all together into a string!
var htmlOutput = '<time datetime=\"' + numericalDate + '\">' + displayDate + '</time>';
console.log(htmlOutput);